# BBOARD: Board game search application


![Generic badge](https://img.shields.io/badge/license-MIT-blue.svg)
![Generic badge](https://img.shields.io/badge/php-%3E%3D8.0.16-green)
![Generic badge](https://img.shields.io/bitbucket/pr/rebelangel/wdpai)





## Overview
In the app you can make a digital list of your board games. You can then search for one among your games according to a number of criteria such as play time, number of players, minimum age or difficulty level.

The moderator can add new games to the database using a special view so that users can then add them to their accounts.

## Technologies Used
- HTML5
- PHP
- CSS
- Javascript
- PostgreSQL
- Docker

## Screenshots
![Login screen](screenshots/LoginScreen.PNG)
![Home Screen](screenshots/homeScreen.PNG)
![Game View](screenshots/GameView.PNG)
![Search View](screenshots/SearchScreen.PNG)

## Installation
-Download or clone this repository

-Create a file:
```config.php``` 
in root
and fill it with database connection credentials like:
```
<?php
const USERNAME = 'username';
const PASSWORD = 'password';
const HOST = 'host name';
const DATABASE = 'database name';
```


-Use docker to build app with command:
```docker-compose build``` =>
```docker-compose up```

-Go to adress in browser:
```localhost:8080/index```


## LICENSE
MIT License

Copyright (c) [2022] [Radosław Suder]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.







