/*
 Navicat PostgreSQL Data Transfer

 Source Server         : Wdpai
 Source Server Type    : PostgreSQL
 Source Server Version : 140004
 Source Host           : ec2-34-242-84-130.eu-west-1.compute.amazonaws.com:5432
 Source Catalog        : dfh5hatit516eu
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 140004
 File Encoding         : 65001

 Date: 26/06/2022 20:43:20
*/


-- ----------------------------
-- Sequence structure for difficulty_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."difficulty_id_seq";
CREATE SEQUENCE "public"."difficulty_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for games_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."games_id_seq";
CREATE SEQUENCE "public"."games_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for userRoles_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."userRoles_id_seq";
CREATE SEQUENCE "public"."userRoles_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for users_Id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."users_Id_seq";
CREATE SEQUENCE "public"."users_Id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Table structure for difficulty
-- ----------------------------
DROP TABLE IF EXISTS "public"."difficulty";
CREATE TABLE "public"."difficulty" (
  "id" int4 NOT NULL DEFAULT nextval('difficulty_id_seq'::regclass),
  "difficulty_level" varchar(100) COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Records of difficulty
-- ----------------------------
INSERT INTO "public"."difficulty" VALUES (1, 'Niski');
INSERT INTO "public"."difficulty" VALUES (2, 'Średni');
INSERT INTO "public"."difficulty" VALUES (3, 'Wysoki');

-- ----------------------------
-- Table structure for games
-- ----------------------------
DROP TABLE IF EXISTS "public"."games";
CREATE TABLE "public"."games" (
  "id" int4 NOT NULL DEFAULT nextval('games_id_seq'::regclass),
  "title" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "minimum_time_minute" int4 NOT NULL,
  "minimum_age" int4 NOT NULL,
  "average_time_minute" int4 NOT NULL,
  "minimum_players" int4 NOT NULL,
  "maximum_players" int4 NOT NULL,
  "image" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "id_difficulty" int4
)
;

-- ----------------------------
-- Records of games
-- ----------------------------
INSERT INTO "public"."games" VALUES (1, 'Catan', 60, 10, 90, 3, 4, 'catan.jpg', 2);
INSERT INTO "public"."games" VALUES (3, 'Warcaby', 15, 7, 25, 2, 2, 'checkers.jpg', 1);
INSERT INTO "public"."games" VALUES (4, 'BattleStar Galactica', 120, 14, 150, 3, 6, 'battlestarGalactica.jpg', 3);
INSERT INTO "public"."games" VALUES (5, 'Small World', 40, 8, 60, 2, 5, 'smallworld.jpg', 2);
INSERT INTO "public"."games" VALUES (7, 'Uno', 15, 6, 25, 2, 10, 'uno.png', 1);
INSERT INTO "public"."games" VALUES (6, 'Fallout Shelter', 60, 14, 80, 2, 4, 'falloutshelter.png', 2);
INSERT INTO "public"."games" VALUES (17, 'Arkham Horror 3ed', 120, 14, 180, 1, 6, 'pol_pl_Horror-w-Arkham-trzecia-edycja-5787_2.jpg', 2);
INSERT INTO "public"."games" VALUES (2, 'Szachy', 20, 7, 40, 2, 2, 'chess.jpg', 2);

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS "public"."roles";
CREATE TABLE "public"."roles" (
  "id" int4 NOT NULL DEFAULT nextval('"userRoles_id_seq"'::regclass),
  "role" varchar(100) COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO "public"."roles" VALUES (1, 'Admin');
INSERT INTO "public"."roles" VALUES (2, 'Moderator');
INSERT INTO "public"."roles" VALUES (3, 'User');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS "public"."users";
CREATE TABLE "public"."users" (
  "id" int4 NOT NULL DEFAULT nextval('"users_Id_seq"'::regclass),
  "name" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "email" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "password" varchar(255) COLLATE "pg_catalog"."default",
  "id_role" int4 NOT NULL
)
;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO "public"."users" VALUES (7, 'Radek', 'test@pk.edu.pl', '$2y$10$lExXDB1ckTkK9mNziBbC9ObF1zWO6xxKL4H52SlVClqWgPPsOoDui', 3);
INSERT INTO "public"."users" VALUES (9, 'John', 'john.snow@pk.edu.pl', '$2y$10$2QxEDDebWJ.nRzpM5nciZuEc/vDPqhOTZKAM4dyFIlH2y26Ap3qdu', 3);
INSERT INTO "public"."users" VALUES (10, 'Maria', 'maria@gmail.com', '$2y$10$0w52qN.UEsGZbej7r70S8O34uRvjOEJqK9RtCVTKdgF7Px1iLQMae', 3);
INSERT INTO "public"."users" VALUES (11, 'Michal', 'michal@gmail.com', '$2y$10$oBoP1zcTnAxiteB8uRhMvORhjIq46vfjK6/iEEteyq6zwifbVFdrC', 3);
INSERT INTO "public"."users" VALUES (8, 'Radek', 'rad@gmail.com', '$2y$10$ctodHKFZuql9EewrTrzu6OVLgjiIED/hYJmCyzMKxTKrEYe4cY0pS', 2);

-- ----------------------------
-- Table structure for users_games
-- ----------------------------
DROP TABLE IF EXISTS "public"."users_games";
CREATE TABLE "public"."users_games" (
  "id_user" int4 NOT NULL,
  "id_game" int4 NOT NULL
)
;

-- ----------------------------
-- Records of users_games
-- ----------------------------
INSERT INTO "public"."users_games" VALUES (7, 5);
INSERT INTO "public"."users_games" VALUES (9, 1);
INSERT INTO "public"."users_games" VALUES (9, 3);
INSERT INTO "public"."users_games" VALUES (9, 4);
INSERT INTO "public"."users_games" VALUES (9, 5);
INSERT INTO "public"."users_games" VALUES (9, 7);
INSERT INTO "public"."users_games" VALUES (9, 6);
INSERT INTO "public"."users_games" VALUES (9, 2);
INSERT INTO "public"."users_games" VALUES (10, 3);
INSERT INTO "public"."users_games" VALUES (10, 7);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."difficulty_id_seq"
OWNED BY "public"."difficulty"."id";
SELECT setval('"public"."difficulty_id_seq"', 3, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."games_id_seq"
OWNED BY "public"."games"."id";
SELECT setval('"public"."games_id_seq"', 17, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."userRoles_id_seq"
OWNED BY "public"."roles"."id";
SELECT setval('"public"."userRoles_id_seq"', 3, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."users_Id_seq"
OWNED BY "public"."users"."id";
SELECT setval('"public"."users_Id_seq"', 11, true);

-- ----------------------------
-- Indexes structure for table difficulty
-- ----------------------------
CREATE UNIQUE INDEX "difficulty_difficulty_uindex" ON "public"."difficulty" USING btree (
  "difficulty_level" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE UNIQUE INDEX "difficulty_id_uindex" ON "public"."difficulty" USING btree (
  "id" "pg_catalog"."int4_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table difficulty
-- ----------------------------
ALTER TABLE "public"."difficulty" ADD CONSTRAINT "difficulty_pk" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table games
-- ----------------------------
CREATE UNIQUE INDEX "games_id_uindex" ON "public"."games" USING btree (
  "id" "pg_catalog"."int4_ops" ASC NULLS LAST
);
CREATE UNIQUE INDEX "games_image_uindex" ON "public"."games" USING btree (
  "image" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE UNIQUE INDEX "games_title_uindex" ON "public"."games" USING btree (
  "title" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table games
-- ----------------------------
ALTER TABLE "public"."games" ADD CONSTRAINT "games_pk" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table roles
-- ----------------------------
CREATE UNIQUE INDEX "userroles_id_uindex" ON "public"."roles" USING btree (
  "id" "pg_catalog"."int4_ops" ASC NULLS LAST
);
CREATE UNIQUE INDEX "userroles_role_uindex" ON "public"."roles" USING btree (
  "role" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table roles
-- ----------------------------
ALTER TABLE "public"."roles" ADD CONSTRAINT "userroles_pk" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table users
-- ----------------------------
CREATE UNIQUE INDEX "users_id_uindex" ON "public"."users" USING btree (
  "id" "pg_catalog"."int4_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table users
-- ----------------------------
ALTER TABLE "public"."users" ADD CONSTRAINT "users_pk" PRIMARY KEY ("id");

-- ----------------------------
-- Foreign Keys structure for table games
-- ----------------------------
ALTER TABLE "public"."games" ADD CONSTRAINT "games_difficulty_id_fk" FOREIGN KEY ("id_difficulty") REFERENCES "public"."difficulty" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table users
-- ----------------------------
ALTER TABLE "public"."users" ADD CONSTRAINT "users_userroles_id_fk" FOREIGN KEY ("id_role") REFERENCES "public"."roles" ("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table users_games
-- ----------------------------
ALTER TABLE "public"."users_games" ADD CONSTRAINT "games_users_games___fk" FOREIGN KEY ("id_game") REFERENCES "public"."games" ("id") ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE "public"."users_games" ADD CONSTRAINT "user_users_games___fk" FOREIGN KEY ("id_user") REFERENCES "public"."users" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
