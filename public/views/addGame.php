<?php
if (session_status() != PHP_SESSION_ACTIVE){
    session_start();
}
//session_start();
if(!isset($_SESSION['email'])) {
    header("location:logout");
}
?>

<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type = "text/css" href="public/css/styleNavMenu.css">
    <link rel="stylesheet" type = "text/css" href="public/css/styleAddGame.css">
    <script src="https://kit.fontawesome.com/06bfc23a09.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src = "./public/js/search.js" defer>  </script>



    <title>myGames</title>
</head>
<body>
<div class="base-container">
    <nav class = "navigate">
        <div class ="logo">
            <img src="public/img/bboardLogoCut.png">
        </div>


        <div class = "buttons" id = "navMenu">
            <form class = "myGames" action = "myGames" method = "GET">
                <button>Moje gry</button>
            </form>
            <form class = "search" action = "search" method = "GET">
                <button>Wyszukaj</button>
            </form>
            <form class = "whereToBuyGames" action = "whereToBuyGames" method = "GET">
                <button>Gdzie kupić grę</button>
            </form>
            <form class = "logout" action="logout" method="POST">
                <button>Wyloguj</button>
            </form>
        </div>

        <div class = "burger">
            <button>MENU</button>
        </div>

        <script type="text/javascript" src = "./public/js/hamMenu.js">  </script>

    </nav>
    <main>
        <div class="messages">
            <?php if(isset($messages)){
                foreach ($messages as $message){
                    echo $message;
                }
            }
            ?>
        </div>

        <div class="searchBar">
            <p>Dodaj swoją nową grę!</p>
            <input placeholder="wyszukaj grę w bazie">

        </div>

        <section class="games">

            <?php foreach ($games as $game) : ?>
                <form class="addGame" action = "addGameToClient" method = "POST">
                    <div class = "titleOfGames">
                        <input class ="addGameToCli" type = "submit" name = "gameTitle" value ="<?= $game['title']; ?>">

                    </div>
                </form>

            <?php endforeach; ?>

        </section>
    </main>
</div>

</body>

<template id="game-Template">
    <form class="addGame" action = "addGameToClient" method = "POST">
        <div class = "titleOfGames">
            <input class ="addGameToCli" type = "submit" name = "gameTitle" value ="">
        </div>
    </form>

</template>

</html>