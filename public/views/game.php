<?php
if (!session_status() == PHP_SESSION_ACTIVE){
    session_start();
}
if(!isset($_SESSION)) {
    header("location:logout");
}
?>
<!DOCTYPE html>

<head>
    <link rel="stylesheet" type = "text/css" href="public/css/styleGame.css">
    <link rel="stylesheet" type = "text/css" href="public/css/styleNavMenu.css">

    <script src="https://kit.fontawesome.com/06bfc23a09.js" crossorigin="anonymous"></script>


    <title><?= $game->getTitle(); ?></title>
</head>

<body>
<div class="base-container">
    <nav class = "navigate">
        <div class ="logo">
            <img src="public/img/bboardLogoCut.png">
        </div>


        <div class = "buttons" id = "navMenu">
            <form class = "myGames" action = "myGames" method = "GET">
                <button>Moje gry</button>
            </form>
            <form class = "search" action = "search" method = "GET">
                <button>Wyszukaj</button>
            </form>
            <form class = "whereToBuyGames" action = "whereToBuyGames" method = "GET">
                <button>Gdzie kupić grę</button>
            </form>
            <form class = "logout" action="logout" method="POST">
                <button>Wyloguj</button>
            </form>
        </div>

        <div class = "burger">
            <button>MENU</button>
        </div>


        <script type="text/javascript" src = "./public/js/hamMenu.js">  </script>
    </nav>
    <main>
        <div class =  "TitleAndImage">
            <div class ="Image">
                <img src="public/img/uploads/<?= $game->getImage() ?>" >
            </div>
            <div class ="Title">
                <p>
                    <?= $game->getTitle(); ?>
                </p>
            </div>

        </div>

        <div class =  "propertiesOfGame">
            <div class =  "Age">
                <i class="fa-solid fa-child-reaching fa-3x"></i>
                <p>Wiek</p>
                <p><?= $game->getMinimumAge(); ?>+</p>

            </div>
            <div class = "Time">
                <i class = "fa-regular fa-clock fa-3x"></i>
                <p>Czas</p>
                <p><?= $game->getMinimumTimeMinute(); ?>'+</p>


            </div>
            <div class = "NumberOfPlayers">
                <i class = "fa-solid fa-user-plus fa-3x"></i>
                <p>Ilość Graczy</p>
                <p><?= $game->getMinimumPlayers(); ?> - <?= $game->getMaximumPlayers(); ?></p>


            </div>
            <div class = "Difficulty">
                <i class = "fa-solid fa-fire fa-3x"></i>
                <p>Poziom Trudności</p>
                <p><?= $game->getDifficultyLevel(); ?></p>

            </div>

        </div>



    </main>
</div>

</body>
