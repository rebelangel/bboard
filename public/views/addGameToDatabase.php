<?php
if (!session_status() == PHP_SESSION_ACTIVE){
    session_start();
}
if(!isset($_SESSION)) {
    header("location:logout");
}
?>
<!DOCTYPE html>

<head>
    <link rel="stylesheet" type = "text/css" href="public/css/styleAddGameToDatabase.css">
    <link rel="stylesheet" type = "text/css" href="public/css/styleNavMenu.css">

    <script src="https://kit.fontawesome.com/06bfc23a09.js" crossorigin="anonymous"></script>


    <title><ADD GAME</title>
</head>

<body>
<div class="base-container">
    <nav class = "navigate">
        <div class ="logo">
            <img src="public/img/bboardLogoCut.png">
        </div>


        <div class = "buttons" id = "navMenu">
            <form class = "logout" action="logout" method="POST">
                <button>Wyloguj</button>
            </form>
        </div>

        <div class = "burger">
            <button>MENU</button>
        </div>


        <script type="text/javascript" src = "./public/js/hamMenu.js">  </script>
    </nav>
    <main>
        <section class="add-games-form">
            <p>Wprowadź nową grę do bazy danych</p>
            <form action="addGameToDatabase" method="POST" ENCTYPE="multipart/form-data">
                <div class="messages">
                    <?php
                    if(isset($messages)){
                        foreach($messages as $message) {
                            echo $message;
                        }
                    }
                    ?>
                </div>
                <div class="long-forms">
                    <input name="title" type="text" placeholder="Nazwa gry">
                    <input name="age" type="number" placeholder="Minimalny wiek">
                </div>
                <div class="short-forms-first">
                    <input name="minTime" type="number" placeholder="Minimalny czas gry">
                    <input name="maxTime" type="number" placeholder="Średni czas gry">
                </div>
                <div class="short-forms-second">
                    <input name="minPlayers" type="number" placeholder="Minimalna liczba graczy">
                    <input name="maxPlayers" type="number" placeholder="Maksymalna liczba graczy">
                </div>
                <div class ="difficulty-level">
                    <p>Poziom trudności gry</p>
                    <select id = "dropDown-1" input name="difficulty">
                        <option value="1">Niski</option>
                        <option value="2" selected>Średni</option>
                        <option value="3" >Wysoki</option>
                    </select>
                </div>


                <div class="fileAdd">
                    <input type="file" name="file" id="img" style="display:none;"/>

                    <label for="img">
                        <i class="fa-solid fa-square-plus fa-3x"></i>
                        <p>Dodaj zdjęcie gry</p>


                    </label>


                </div>
                <button type="submit">Akceptuj</button>
            </form>
        </section>

    </main>
</div>

</body>
