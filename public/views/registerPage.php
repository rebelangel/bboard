<!DOCTYPE html>
<head>
    <link rel="stylesheet" type = "text/css" href="public/css/styleRegister.css">
    <title>REGISTER</title>
</head>

<body>
<div class="container">
    <div class="logo">
        <img src="public/img/bbLogo1.png">
    </div>
    <div class="register-container">
        <form class="register" action="register" method="POST">
            <div class="messages">
                <?php
                if(isset($messages)){
                    foreach($messages as $message) {
                        echo $message;
                    }
                }
                ?>
            </div>
            <input name="email" type="text" placeholder="email@email.com">
            <input name="password" type="password" placeholder="hasło">
            <input name="confirmedPassword" type="password" placeholder="powtórz hasło">
            <input name="name" type="text" placeholder="imię">
            <button type="submit">ZAREJESTRUJ SIĘ</button>
        </form>
    </div>
</div>
</body>