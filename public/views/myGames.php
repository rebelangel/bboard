<?php
if (session_status() != PHP_SESSION_ACTIVE){
    session_start();
}
//session_start();
if(!isset($_SESSION['email'])) {
    header("location:logout");
}
?>

<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type = "text/css" href="public/css/styleNavMenu.css">
    <link rel="stylesheet" type = "text/css" href="public/css/styleMyGames.css">
    <script src="https://kit.fontawesome.com/06bfc23a09.js" crossorigin="anonymous"></script>



    <title>myGames</title>
</head>
<body>
    <div class="base-container">
        <nav class = "navigate">
            <div class ="logo">
                <img src="public/img/bboardLogoCut.png">
            </div>


            <div class = "buttons" id = "navMenu">
                <form class = "myGames" action = "myGames" method = "GET">
                    <button>Moje gry</button>
                </form>
                <form class = "search" action = "search" method = "GET">
                    <button>Wyszukaj</button>
                </form>
                <form class = "whereToBuyGames" action = "whereToBuyGames" method = "GET">
                    <button>Gdzie kupić grę</button>
                </form>
                <form class = "logout" action="logout" method="POST">
                    <button>Wyloguj</button>
                </form>
            </div>

            <div class = "burger">
                <button>MENU</button>
            </div>

            <script type="text/javascript" src = "./public/js/hamMenu.js">  </script>

        </nav>
        <main>
            <div class="messages">
                <?php if(isset($messages)){
                    foreach ($messages as $message){
                        echo $message;
                    }
                }
                ?>
            </div>

            <?php if(!isset($search)) : ?>
            <form class="togglesearch" action="newGameSearch" method="POST">
                <p>Dodaj swoją nową grę!</p>
                <input class="searchInput" type="text" placeholder="" id="searchBar" name="searchBar">
                <button type ="submit">WYSZUKAJ</button>

            </form>
            <?php endif ?>





            <section class="games">
                <?php if(!isset($search)) : ?>
                    <div class="add" >
                    <div class = "icon" >
                        <button class="btn" id = "searchButton">
                            <i class="fa-solid fa-square-plus fa-10x"></i>
                        </button>
                    </div >
                </div >

                    <script type="text/javascript" src = "./public/js/searchBar.js">  </script>


                <?php endif ?>

                <?php foreach ($games as $game) : ?>
                    <form class="game-1" action = "showGame" method = "POST">
                        <input type="hidden" name = "gameTitle" value="<?=$game->getTitle()?>">
                        <input type ="image" src="public/img/uploads/<?= $game->getImage() ?>">
                        <div class = "title">
                            <input type = "submit" name = "gameTitle" value ="<?= $game->getTitle(); ?>"></input>
                        </div>
                    </form>

                <?php endforeach; ?>

            </section>
        </main>
</div>

</body>
</html>