const search = document.querySelector('input[placeholder="wyszukaj grę w bazie"]');
const gameContainer = document.querySelector(".games");
search.addEventListener("keyup", function (event) {
    if (event.key === "Enter") {
        event.preventDefault();

        const data = {search: this.value};

        fetch("./searchForGamesAPI", {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then(function (response) {
            // console.log(response);

            return response.json();
        }).then(function (games) {


            gameContainer.innerHTML = "";
            loadGames(games)
        });
    }
});


function loadGames(games){
    games.forEach(game => {
        createGame(game);
    })
}

function createGame(game){
const template = document.querySelector("#game-Template");

const clone = template.content.cloneNode(true);
const title = clone.querySelector(".addGameToCli" );

title.value = game.title;

    gameContainer.appendChild(clone);

}