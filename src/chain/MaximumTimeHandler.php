<?php
require_once 'AbstractHandler.php';

class MaximumTimeHandler extends AbstractHandler
{
    private int $averageTime;


    public function __construct(int $time)
    {
        $this->averageTime = $time;

    }



    protected function filter(array $request): array
    {
        $arrayToReturn = array();
        foreach ($request as $oneRequest){
            if($oneRequest->getAverageTimeMinute() >= $this->averageTime){

                array_push($arrayToReturn,$oneRequest);
            }

        }

        return $arrayToReturn;

    }


}