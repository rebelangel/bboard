<?php
require_once 'AbstractHandler.php';

class MinimumTimeHandler extends AbstractHandler
{
    private int $minimumTime;


    public function __construct(int $time)
    {
        $this->minimumTime = $time;

    }

    protected function filter(array $request): array
    {
        $arrayToReturn = array();
        foreach ($request as $oneRequest){
            if($oneRequest->getMinimumTimeMinute() <= $this->minimumTime){
                array_push($arrayToReturn,$oneRequest);
            }

        }
        return $arrayToReturn;
    }


}