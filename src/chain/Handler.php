<?php
//require_once 'Game.php';


interface Handler
{
    public function setNext(Handler $handler ): Handler;

    public function handle(array $request ): array;
}