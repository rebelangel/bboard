<?php

require_once 'AppController.php';

class DefaultController extends AppController{

    public function index(){
        $this->render('login');
    }
    
    public function home() {
        $this->render('home');

    }

    public function myGames() {
        $this->render('myGames');

    }
    public function search() {
        $this->render('search');

    }
    public function whereToBuyGames() {
    $this->render('whereToBuyGames');


    }
    public function registerPage() {
        $this->render('registerPage');
    }

    
}