<?php

require_once 'AppController.php';
require_once __DIR__.'/../models/Game.php';
require_once __DIR__.'/../repository/GameRepository.php';
require_once __DIR__.'/../chain/MinimumTimeHandler.php';
require_once __DIR__.'/../chain/MaximumTimeHandler.php';
require_once __DIR__.'/../chain/NumberOfPlayersHandler.php';
require_once __DIR__.'/../chain/DifficultyHandler.php';
require_once __DIR__.'/../chain/MinimumAgeHandler.php';





class GameController extends AppController
{
    const MAX_FILE_SIZE = 1024*1024;
    const SUPPORTED_TYPES = ['image/png', 'image/jpeg'];
    const UPLOAD_DIRECTORY = '/../public/img/uploads/';


    private function checkSession(){
        if (session_status() != PHP_SESSION_ACTIVE){
            session_start();
        }
        if(!isset($_SESSION['email'])) {
            header("location:logout");
        }
    }

    private function checkPost(){
        if($this->isPost() !== true){
            return $this->render('login');
        }
    }


    public function myGames()
    {
        $this->checkSession();

        $gameRepository = new GameRepository();
        $games = $gameRepository->getGames($_SESSION['email']);
        $this->render('myGames',['games' => $games]);




    }

    public function searchForGames(){
        $this->checkPost();
        $this->checkSession();


        $lowSlider = $_POST["low-slider"];
        $highSlider = $_POST["high-slider"];
        $players = $_POST["players"];
        $age = $_POST["age"];
        $difficulty = $_POST["difficulty"];


        $gameRepository = new GameRepository();
        $allMyGames = $gameRepository->getGames($_SESSION['email']);

        $MinTH = new MinimumTimeHandler($lowSlider);
        $MaxTH = new MaximumTimeHandler($highSlider);
        $PlayerH = new NumberOfPlayersHandler($players);
        $MinAgeH = new MinimumAgeHandler($age);
        $DiffH = new DifficultyHandler($difficulty);

        $MinTH->setNext($MaxTH);
        $MaxTH->setNext($PlayerH);
        $PlayerH->setNext($MinAgeH);
        $MinAgeH->setNext($DiffH);



        $gamesToShow = $MinTH->handle($allMyGames);

        if($gamesToShow){
            $this->render('myGames',['games' => $gamesToShow, 'search' => ['true']]);
        }else{
            $this->render('myGames',['games' => $gamesToShow, 'messages' => ['Brak gier o podanych parametrach'] , 'search' => ['true']]);
        }

    }
    public function showGame(){
        $this->checkPost();
        $this->checkSession();


        $gameRepository = new GameRepository();
        $game = $gameRepository->getOneGameFromTitle($_POST['gameTitle']);


        $this->render('game',['game' => $game]);

    }

    public function newGameSearch(){
        $this->checkPost();
        $this->checkSession();

        $gameRepository = new GameRepository();
        $games = $gameRepository->searchForGamesInDatabase($_POST['searchBar'],$_SESSION['email']);

        $this->render('addGame',['games' => $games]);



    }

    public function addGameToClient(){
        $this->checkPost();
        $this->checkSession();
            $gameRepository = new GameRepository();

            $gameRepository->addGameToClient($_POST['gameTitle'],$_SESSION['email']);
            $this->myGames();




    }


    public function addGameToDatabase()
    {
        $this->checkPost();
        $this->checkSession();

        $title = $_POST['title'];
        $fileName = $_FILES['file']['name'];
        $minTime = $_POST['minTime'];
        $avgTime = $_POST['maxTime'];
        $age = $_POST['age'];
        $minPlayers = $_POST['minPlayers'];
        $maxPlayers = $_POST['maxPlayers'];
        $difficultyLevel = $_POST['difficulty'];


        if(!$title){
            return $this->render('addGameToDatabase',['messages' => ['Nie wpisano tytułu']]);
        }
        if(!$minTime or $minTime<=0){
            return $this->render('addGameToDatabase',['messages' => ['Nie wpisano minimalnego czasu gry']]);
        }
        if(!$avgTime or $avgTime<=0){
            return $this->render('addGameToDatabase',['messages' => ['Nie wpisano średniego czasu gry']]);
        }
        if(!$age or $age<=0){
            return $this->render('addGameToDatabase',['messages' => ['Nie wpisano minimalnego wieku graczy']]);
        }
        if(!$minPlayers or $minPlayers<=0){
            return $this->render('addGameToDatabase',['messages' => ['Nie wpisano minimalnej ilosci graczy']]);
        }
        if(!$maxPlayers or $maxPlayers < $minPlayers){
            return $this->render('addGameToDatabase',['messages' => ['Nie wpisano maksymalnej ilosci graczy']]);
        }





        if ($this->isPost() && is_uploaded_file($_FILES['file']['tmp_name']) && $this->validate($_FILES['file'])) {
            $game = new Game($title, $fileName, $minTime, $avgTime, $age, $minPlayers, $maxPlayers,$difficultyLevel);
            $gameRepository = new GameRepository();
            if($gameRepository->addGameToDatabase($game)){
                move_uploaded_file(
                    $_FILES['file']['tmp_name'],
                    dirname(__DIR__) . self::UPLOAD_DIRECTORY . $_FILES['file']['name']
                );
                return $this->render('addGameToDatabase',['messages' => ['Pomyślnie dodano grę do bazy danych']]);



            }


        }else{
            return $this->render('addGameToDatabase',['messages' => ['Nie udało się dodać gry do bazy danych']]);

        }


    }

    public function searchForGamesAPI(){
        $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';

        if ($contentType === "application/json") {
            $this->checkSession();
            $content = trim(file_get_contents("php://input"));
            $decoded = json_decode($content, true);

            header('Content-type: application/json');
            http_response_code(200);
            $repository = new GameRepository();

            echo json_encode( $repository->searchForGamesInDatabaseForAPI($decoded['search'],$_SESSION['email']));
        }

    }

    private function validate(array $file): bool
    {
        if ($file['size'] > self::MAX_FILE_SIZE) {
            $this->message[] = 'File is too large for destination file system.';
            return false;
        }

        if (!isset($file['type']) || !in_array($file['type'], self::SUPPORTED_TYPES)) {
            $this->message[] = 'File type is not supported.';
            return false;
        }
        return true;
    }




}