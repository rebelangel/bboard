<?php

require_once 'AppController.php';
require_once __DIR__.'/../models/User.php';
require_once __DIR__.'/../repository/UserRepository.php';

class SecurityController extends AppController
{
    public function login()
    {
        $userRepository = new UserRepository();

        if($this->isPost() !== true){
            return $this->render('login');
        }


        $email = $_POST["email"];
        $password = $_POST["password"];


        $user = $userRepository->getUser($email);

        if(!$user){
            return $this->render('login',['messages' => ['Nie ma takiego użytkownika']]);
        }


        if ($user->getEmail() !== $email){
            return $this->render('login',['messages' => ['Nie ma użytkownika powiązanego z tym adresem email']]);
        }


        if (!password_verify($password, $user->getPassword())){
            return $this->render('login',['messages' => ['Złe hasło']]);
        }

        $cookie_name = $user->getName();

        session_start();

        $_SESSION['email'] = $email;
        setcookie("user",$cookie_name,time() + (86400 * 30));

        if($userRepository->checkUserRole($email) == 'Moderator'){
            return $this->render('addGameToDatabase');
        }


        $gameRepo = new GameRepository();
        $games = $gameRepo->getGames($email);
        $numberOfGames = count($games);

        if($numberOfGames == 0 ){
            return $this->render('home',['messages' => ['Na razie nie masz gier do wyświelenia. Spróbuj dodać swoją pierwszą grę.'],'games' => $games ]);
        }

        if($numberOfGames == 1 ){
            return $this->render('home',['games' => $games]);
        }

        if($numberOfGames == 2){
            $randKeys = array_rand($games,2);
            $gamesToShow = array($games[$randKeys[0]],$games[$randKeys[1]]);
            return $this->render('home',['games' => $gamesToShow]);
        }

        if($numberOfGames >=3 ){
            $randKeys = array_rand($games,3);
            $gamesToShow = array($games[$randKeys[0]], $games[$randKeys[1]],$games[$randKeys[2  ]]);
            return $this->render('home',['games' => $gamesToShow]);
        }


    }

    public function logout(){
        session_start();

        setcookie("user","",time() -10    );

        session_destroy();
        return $this->render('login');


    }

    public function register(){
        $userRepository = new UserRepository();

        if($this->isPost() !== true){
            return $this->render('login');
        }


        $email = $_POST["email"];
        $password = $_POST["password"];
        $repeatedPassword = $_POST["confirmedPassword"];
        $name = $_POST["name"];


        if(!$email){
            return $this->render('registerPage',['messages' => ['Nie wpisano adresu email']]);
        }
        if(!$password){
            return $this->render('registerPage',['messages' => ['Nie wpisano hasla']]);
        }
        if(!$repeatedPassword){
            return $this->render('registerPage',['messages' => ['Nie powtórzono hasła']]);
        }
        if(!$name){
            return $this->render('registerPage',['messages' => ['Nie wpisano imienia']]);
        }
        if($password != $repeatedPassword){
            return $this->render('registerPage',['messages' => ['Hasła się nie zgadzają']]);
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
            return $this->render('registerPage',['messages' => ['Zły format adresu email']]);
        }

        $user = $userRepository->getUser($email);

        if($user){
            return $this->render('registerPage',['messages' => ['Podany adres email jest już wykorzystywany']]);
        }

        $userRepository->registerUser($email,password_hash($password,PASSWORD_BCRYPT),$name);

        return $this->render('login',['messages' => ['Zarejestrowano użytkownika']]);











    }

}