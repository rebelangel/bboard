<?php
require_once "Repository.php";
require_once __DIR__.'/../models/Game.php';
class GameRepository extends Repository
{
    public function getGames(string $email): array
    {
        $stmt = $this->database->connect()->prepare(
            'SELECT title,minimum_time_minute,average_time_minute,minimum_age,minimum_players,maximum_players,image,difficulty_level from games join users_games ON games.id = users_games.id_game join users on users_games.id_user = users.id JOIN difficulty on difficulty.id = games.id_difficulty where email = :email order by title'
        )   ;
        $stmt->bindParam(':email',$email,PDO::PARAM_STR);
        $stmt->execute();
        $arrayOfGames = array();


        do {
            $game = $stmt->fetch(PDO::FETCH_ASSOC);
//            print_r($game);
            if($game != false) {
                $arrayOfGames[] = new Game(
                    $game['title'],
                    $game['image'],
                    $game['minimum_time_minute'],
                    $game['average_time_minute'],
                    $game['minimum_age'],
                    $game['minimum_players'],
                    $game['maximum_players'],
                    $game['difficulty_level']
                );
            }
        }while($game != false);



        return $arrayOfGames;
    }

    public function getOneGameFromTitle(string $title){
        $stmt = $this->database->connect()->prepare(
            'SELECT title,minimum_time_minute,average_time_minute,minimum_age,minimum_players,maximum_players,image,difficulty_level FROM "games" join difficulty on games.id_difficulty = difficulty.id WHERE title= :title '
        )   ;
        $stmt->bindParam(':title',$title,PDO::PARAM_STR);
        $stmt->execute();

        $game = $stmt->fetch(PDO::FETCH_ASSOC);


            $gameToReturn = new Game(
                $game['title'],
                $game['image'],
                $game['minimum_time_minute'],
                $game['average_time_minute'],
                $game['minimum_age'],
                $game['minimum_players'],
                $game['maximum_players'],
                $game['difficulty_level']
            );
            return $gameToReturn;

    }

    public function searchForGamesInDatabase(string $title, string $email )
    {
        $stmt = $this->database->connect()->prepare(
            '   SELECT title from games where title not in (SELECT title from games join users_games ON games.id = users_games.id_game join users on users_games.id_user = users.id where email = :email) and LOWER(title) LIKE LOWER(:title)'
        );
        $gameToSearch = "%" .strtolower($title)  . "%";
        $stmt->bindParam(':title', $gameToSearch, PDO::PARAM_STR);
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);


        $stmt->execute();

        $arrayOfGames = array();

        do {
            $game = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($game != false) {
                $arrayOfGames[] = $game;
            }


        } while ($game != false);
        return $arrayOfGames;
    }

    public function addGameToClient(string $title, string $email){
        $stmt = $this->database->connect()->prepare(
            '   select id from games where title=:title ;
'
        );
        $stmt->bindParam(':title', $title, PDO::PARAM_STR);


        $stmt->execute();

        $gameID = $stmt->fetch(PDO::FETCH_ASSOC);

        $stmt = $this->database->connect()->prepare(
            '   Select id from users where email = :email ; ;
'
        );
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);


        $stmt->execute();
        $userID = $stmt->fetch(PDO::FETCH_ASSOC);

        $stmt = $this->database->connect()->prepare(
            ' insert into users_games (id_user,id_game) VALUES (:idUser,:idGame)'
        );
        $stmt->bindParam(':idUser', $userID['id'], PDO::PARAM_INT);
        $stmt->bindParam(':idGame', $gameID['id'], PDO::PARAM_INT);

        $stmt->execute();




    }

    public function addGameToDatabase(Game $gameToAdd):bool{
        try {
            $connection = $this->database->connect();
            $connection->beginTransaction();
            $sql = 'INSERT into games(title,minimum_time_minute,average_time_minute,minimum_age,minimum_players,maximum_players,image,id_difficulty) values(:title,:minTime, :avgTime, :minAge, :minPlayers, :maxPlayers, :image, :difficulty)';
            $stmt = $connection->prepare($sql);

            $title = $gameToAdd->getTitle();
            $minTime = $gameToAdd->getMinimumTimeMinute();
            $avgTime = $gameToAdd->getAverageTimeMinute();
            $minAge = $gameToAdd->getMinimumAge();
            $minPlayers = $gameToAdd->getMinimumPlayers();
            $maxPlayers = $gameToAdd->getMaximumPlayers();
            $image = $gameToAdd->getImage();
            $difficulty = $gameToAdd->getDifficultyLevel();


            $stmt->bindParam(':title', $title, PDO::PARAM_STR);
            $stmt->bindParam(':minTime', $minTime, PDO::PARAM_INT);
            $stmt->bindParam(':avgTime', $avgTime, PDO::PARAM_INT);
            $stmt->bindParam(':minAge', $minAge , PDO::PARAM_INT);
            $stmt->bindParam(':minPlayers', $minPlayers, PDO::PARAM_INT);
            $stmt->bindParam(':maxPlayers', $maxPlayers, PDO::PARAM_INT);
            $stmt->bindParam(':image', $image, PDO::PARAM_STR);
            $stmt->bindParam(':difficulty', $difficulty, PDO::PARAM_INT);

            $stmt->execute();
//            $stmt->closeCursor();
            $connection->commit();

            return true;



        }catch (PDOException $e) {
            $this->database->connect()->rollBack();
            die($e->getMessage());
        }
    }



    public function searchForGamesInDatabaseForAPI(string $title, string $email )
    {
        $stmt = $this->database->connect()->prepare(
            '   SELECT title from games where title not in (SELECT title from games join users_games ON games.id = users_games.id_game join users on users_games.id_user = users.id where email = :email) and LOWER(title) LIKE LOWER(:title)'
        );

        $gameToSearch = "%" .strtolower($title)  . "%";
        $stmt->bindParam(':title', $gameToSearch, PDO::PARAM_STR);
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);


        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);


    }



}


