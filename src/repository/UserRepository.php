<?php
require_once "Repository.php";
require_once __DIR__.'/../models/User.php';
class UserRepository extends Repository
{
    public function getUser(string $email): ?User
    {
     $stmt = $this->database->connect()->prepare(
         'SELECT * FROM public.users WHERE email = :email'
     )   ;
        $stmt->bindParam(':email',$email,PDO::PARAM_STR);
        $stmt->execute();
        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if($user == false){
            return null;
            //TODO wywalic wyjatek exception lab9 28:50
        }

        return new User(
            $user['email'],
            $user['password'],
            $user['name']
        );
    }

    public function registerUser(string $email,string $password,string $name){
        $stmt = $this->database->connect()->prepare(
            'INSERT into users(name,email,password,id_role) values(:name,:email,:password,:role)'
        )   ;
        $stmt->bindParam(':email',$email,PDO::PARAM_STR);
        $stmt->bindParam(':password',$password,PDO::PARAM_STR);
        $stmt->bindParam(':name',$name,PDO::PARAM_STR);
        $role = 3;
        $stmt->bindParam(':role',$role,PDO::PARAM_INT);



        $stmt->execute();

    }

    public function checkUserRole($email):string{
        $stmt = $this->database->connect()->prepare(
            'SELECT role from users Join roles on users.id_role = roles.id where email=:email'
        )   ;
        $stmt->bindParam(':email',$email,PDO::PARAM_STR);



        $stmt->execute();
        $role = $stmt->fetch(PDO::FETCH_ASSOC);
        return $role['role'];
    }

}